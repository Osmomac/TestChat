package com.volkov.chat.client;


import java.io.*;
import java.net.Socket;
import java.util.Scanner;

public class ChatClient {

    private Socket socket;
    private PrintWriter printWriter;
    private BufferedReader bufferedReader;
    public ChatClient(){

        try {
            socket = new Socket("localhost", 8030);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args){
        ChatClient chatClient = new ChatClient();
        chatClient.runTheApp();
    }

    private void runTheApp() {
        Scanner scanner = new Scanner(System.in);
        try {
            printWriter = new PrintWriter(socket.getOutputStream(),true);
            bufferedReader = new BufferedReader(new InputStreamReader(socket.getInputStream()));
        } catch (IOException e) {
            e.printStackTrace();
        }
        Thread thread =new Thread(() -> {
       while (true){
           try {
              String message = bufferedReader.readLine();
                   System.out.println(message);
           } catch (IOException e) {
               e.printStackTrace();
           }
       }
        });
        thread.start();
        while (true) {
                printWriter.println(socket.getInetAddress().getAddress()+" "+scanner.nextLine());
        }


    }
}
