package com.volkov.chat.server;


import java.io.*;
import java.net.Socket;

public class ThreadServer extends Thread {

    private BufferedReader bufferedReader;
    private PrintWriter printWriter;
    private Socket socket;

    public ThreadServer(Socket socket){
            this.socket = socket;
        try {
            printWriter = new PrintWriter(socket.getOutputStream(),true);
            bufferedReader = new BufferedReader(new InputStreamReader(socket.getInputStream()));

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public BufferedReader getBufferedReader() {
        return bufferedReader;
    }

    public void setBufferedReader(BufferedReader bufferedReader) {
        this.bufferedReader = bufferedReader;
    }

    public PrintWriter getPrintWriter() {
        return printWriter;
    }

    public void setPrintWriter(PrintWriter printWriter) {
        this.printWriter = printWriter;
    }

    public Socket getSocket() {
        return socket;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ThreadServer that = (ThreadServer) o;

        if (bufferedReader != null ? !bufferedReader.equals(that.bufferedReader) : that.bufferedReader != null)
            return false;
        if (printWriter != null ? !printWriter.equals(that.printWriter) : that.printWriter != null) return false;
        return socket != null ? socket.equals(that.socket) : that.socket == null;
    }

    @Override
    public int hashCode() {
        int result = bufferedReader != null ? bufferedReader.hashCode() : 0;
        result = 31 * result + (printWriter != null ? printWriter.hashCode() : 0);
        result = 31 * result + (socket != null ? socket.hashCode() : 0);
        return result;
    }

    public void setSocket(Socket socket) {
        this.socket = socket;
    }

    @Override
    public void run() {
        boolean isRuning = true;
        while (isRuning){
            try {
               String msg = bufferedReader.readLine();
                for (ThreadServer threadServer: ChatServer.threads){
                    if(!threadServer.equals(ThreadServer.this)) {
                    threadServer.getPrintWriter().println(msg);
                }
                }
            } catch (IOException e) {
                e.printStackTrace();
            }finally {
                isRuning=false;
            }


        }

    }
}


